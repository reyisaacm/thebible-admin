import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';

const routes: Routes = [{
  path: 'login',
  loadChildren: './login/login.module#LoginModule',
}, {
  path: '',
  loadChildren: './backend/backend.module#BackendModule',
  canLoad: [AuthGuard],
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
