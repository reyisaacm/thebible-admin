import { Component, OnInit } from '@angular/core';
import {UserLogin} from '../../model/user-login';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  userLoginModel: UserLogin;
  loginForm: FormGroup;
  loginMessage: string;
  processing: boolean;

  constructor(private fb: FormBuilder, private http: HttpClient, private authService: AuthService) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.processing = false;

  }

  ngOnInit() {
  }

  onSubmit(value: UserLogin) {
    this.userLoginModel = new UserLogin();
    this.userLoginModel.username = value.username;
    this.userLoginModel.password = value.password;
    this.loginMessage = 'Logging you in...';
    this.processing = true;
    this.authService.doLogin(this.userLoginModel).subscribe((data: HttpResponse<Response>) => {
      this.processing = false;
      if (data.status === 200) {
        this.loginMessage = 'Success';
      } else {
        this.loginMessage = 'Invalid username/password';
      }
    });
  }

}
