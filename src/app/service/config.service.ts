import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  loginUrl(): string {
    return environment.apiBaseUrl + '/api/auth/login';
  }
}
