import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { UserLogin } from '../model/user-login';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient, private config: ConfigService) { }

  doLogin(userLogin: UserLogin): Observable<HttpResponse<Response>> {
    return this.httpClient.post<Response>(this.config.loginUrl(), userLogin, { observe: 'response' });
  }
  isAuthorized() {
    return false;
  }
}
